# Projet de FWF

### François GOESSENS

## Project links
This project is hosted on render.com :
https://spotivue.onrender.com/#/

Link for the project on Gitlab :
https://gitlab.com/FrancoisGoessens/projet-vue-goessens

## Project description
The goal of this project was to use an API and manipulate all the facets of Vue.js that we saw in class.

After waiting for a certain album to be released, I wondered if Spotify had a category showing the latest projects released in France. Unfortunately, the only category I found was playlists created by Spotify with the latest songs released, but that's not what I wanted.

Therefore, I decided to use the Spotify API and recreate a site similar to the application, mainly to see the latest releases, but also to search for an album to have some information very quickly, as well as the search of artist.