export class Album {
    constructor( id, name, artist, image, date, songs ) {
        this.id = id
        this.name = name
        this.artist = artist
        this.image = image
        this.date = date
        this.songs = songs
    }
}