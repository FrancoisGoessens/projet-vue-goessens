export class Artist {
    constructor( id, name, followers, popularity, genre, image ) {
        this.id = id
        this.name = name
        this.followers = followers
        this.popularity = popularity
        this.genre = genre
        this.image = image
    }
}