import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import SearchAlbum from '../views/SearchAlbum'
import SearchArtist from '../views/SearchArtist'

Vue.use(VueRouter)

const routes = [
    {
        path: '',
        name: 'Home',
        component: Home
    },
    {
        path: '/album',
        name: 'SearchAlbum',
        component: SearchAlbum
    },
    {
        path: '/artist',
        name: 'SearchArtist',
        component: SearchArtist
    }
]

const router = new VueRouter({
    routes
})

export default router
